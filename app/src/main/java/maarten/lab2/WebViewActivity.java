package maarten.lab2;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import java.net.URL;

            // Simple webview activity
public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        WebView myWebView = findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl(prefs.getString("Uri", ""));
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();           // Ends the activity so you can't go back to it
    }
}
