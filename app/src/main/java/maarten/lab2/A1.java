package maarten.lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class A1 extends AppCompatActivity {

    private List<String> headlines;
    private List<String> links;
    private ScheduledExecutorService executor;
    private Runnable runnable;
    private int refreshRate = 0;
    private boolean createdExec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        headlines = new ArrayList();
        links = new ArrayList();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(A1.this);

        switch (prefs.getInt("feedRefresh", -1)) {
            case 0: refreshRate = 10;   break;
            case 1: refreshRate = 60;   break;
            case 2: refreshRate = 1400; break;
        }

        // Creates a runnable to execute in set intervals
        runnable = new Runnable() {
            @Override
            public void run() {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(A1.this);
                final String urlStr = prefs.getString("Url", "");

                headlines.removeAll(headlines);
                links.removeAll(links);

                new RssReader().execute(urlStr);
            }
        };

        // Creates an executor that runs a runnable in set intervals
        if (refreshRate != 0) {			// Does not create one if it's the first time running the app
            executor = Executors.newSingleThreadScheduledExecutor();
            executor.scheduleAtFixedRate(runnable, 0, refreshRate, TimeUnit.MINUTES);
            createdExec = true;			// Used so there are no exceptions when resetting executor
        }

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh);

        // Creates a "force refresh" for debugging purpose
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(A1.this);
                final String urlStr = prefs.getString("Url", "");

                headlines.removeAll(headlines);
                links.removeAll(links);

                new RssReader().execute(urlStr);
            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(A1.this);
        switch (prefs.getInt("feedRefresh", -1)) {
            case 0: refreshRate = 10;   break;
            case 1: refreshRate = 60;   break;
            case 2: refreshRate = 1400; break;
        }

        if (createdExec)
            executor.shutdown();			// Removes all scheduled tasks if they exist

        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(runnable, 0, refreshRate, TimeUnit.MINUTES);
        createdExec = true;
    }

    // Creates the settings menu in the top left corner
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.Preferences) {
        	// Go to the prefrence activity
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.d("getInputStream", "Something went wrong in getInputStream");
            return null;
        }
    }

    // The AsyncTask class that handles getting data from url in the background
    private class RssReader extends AsyncTask<String, Integer, Integer> {
        @Override
        protected Integer doInBackground(String... urlStr) {
            try {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(A1.this);
                final String urlString = prefs.getString("Url", "");

                URL url = new URL(urlString);
                
                // Creates an Xml parser to read the xml file fetched from RSS url
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(false);
                XmlPullParser xpp = factory.newPullParser();

                InputStream stream = getInputStream(url);	// Gets the data returned from url request

                if (stream != null) {
                    xpp.setInput(stream, "UTF_8");

                    boolean insideItem = false;				// For checking, don't want to add page title to headlines

                    int eventType = xpp.getEventType();
                    int maxEntries = 0;

                    switch (prefs.getInt("feedItem", -1)){
                        case 0: maxEntries = 10; Log.d("entries", "set to 10"); break;
                        case 1: maxEntries = 20; Log.d("entries", "set to 20"); break;
                        case 2: maxEntries = 50; Log.d("entries", "set to 50"); break;
                        case 3: maxEntries = 100; Log.d("entries", "set to 100"); break;
                    }

                    int count = 0;

                    while (eventType != XmlPullParser.END_DOCUMENT && count < maxEntries) {

                        if (eventType == XmlPullParser.START_TAG) {		// if in <sometag>

                            if (xpp.getName().equalsIgnoreCase("item") || xpp.getName().equalsIgnoreCase("entry")) {
                                insideItem = true;
                            } 
                            else if (xpp.getName().equalsIgnoreCase("title")) {

                                if (insideItem) {
                                    String temp = new String(xpp.nextText());
                                    Log.d("temp", temp);

                                    if (temp.length() > 1) {			// Some sites have a blank title where there are adds
                                        headlines.add(temp);			// So we don't add those to the lists
                                    } 
                                    else {
                                        insideItem = false;
                                    }
                                }
                            } 
                            else if (xpp.getName().equalsIgnoreCase("link")) {
                                
                                if (insideItem)
                                    links.add(xpp.nextText());
                            }
                        } 
                        else if (eventType == XmlPullParser.END_TAG) {	// if in </sometag>
                            
                            if ((xpp.getName().equalsIgnoreCase("item")) || xpp.getName().equalsIgnoreCase("entry")) {
                                insideItem = false;
                                count++;
                            }
                        }

                        eventType = xpp.next();						// next tag
                    }
                } 
                else {
                   // Displays a toast if unable to get data from url (example: yahoo rss feed)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = null;
                            toast.makeText(A1.this, "Something went wrong, try another url", Toast.LENGTH_LONG).show();
                            SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });

                    return -1;
                }
            }
            catch (MalformedURLException e) {
                Log.d("URL", "Some Malformed URL Exception");
                e.printStackTrace();
                return -1;
            } 
            catch (XmlPullParserException e) {
                Log.d("Parser", "Some XML parser Exception");
                e.printStackTrace();
                return -1;
            } 
            catch (IOException e) {
                Log.d("IO", "Some IO Exception");
                e.printStackTrace();
                return -1;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
            return 0;
        }

        // Creates a new adapter for the listview with updated headlines
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);		// I don't actually do anything with the result, could be void instead

            ArrayAdapter adapter = new ArrayAdapter(A1.this, android.R.layout.simple_list_item_1, headlines);

            ListView list = findViewById(R.id.a1_listView);
            if (result != -1)
                list.setAdapter(adapter);

            list.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(A1.this);
                    final SharedPreferences.Editor editor = prefs.edit();

                    editor.putString("Uri", links.get(position).toString());
                    editor.apply();

                    Intent intent = new Intent(A1.this, WebViewActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
