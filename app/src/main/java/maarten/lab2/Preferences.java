package maarten.lab2;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Preferences extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);

        final Spinner feedItemSpinner = findViewById(R.id.feedItem);
        final Spinner feedRefreshSpinner = findViewById(R.id.feedRefresh);

        // Entries limit spinner
        List<String> feedItemCat = new ArrayList<>();

        feedItemCat.add("10");              // Should use the strings.xml file
        feedItemCat.add("20");
        feedItemCat.add("50");
        feedItemCat.add("100");

        ArrayAdapter<String> feedItemAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, feedItemCat);
        feedItemAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feedItemSpinner.setAdapter(feedItemAdapter);

        // Refresh spinner creation
        List<String> feedRefreshCat = new ArrayList<>();

        feedRefreshCat.add("10 min");       // Should use the strings.xml file
        feedRefreshCat.add("60 min");
        feedRefreshCat.add("Once a day");

        ArrayAdapter<String> feedRefreshAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, feedRefreshCat);
        feedRefreshAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feedRefreshSpinner.setAdapter(feedRefreshAdapter);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final TextView urlText = findViewById(R.id.rssUrl);

        final int feedItem = prefs.getInt("feedItem", -1);
        if (feedItem != -1) {
            feedItemSpinner.setSelection(feedItem);
        }

        final int feedRefresh = prefs.getInt("feedRefresh", -1);
        if (feedRefresh != -1) {
            feedRefreshSpinner.setSelection(feedRefresh);
        }

        final String url = prefs.getString("Url", "");
        if (url != "") {
            urlText.setText(url);
        }

        // Save button
        final Button btn = findViewById(R.id.prefButton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Preferences.this);
                final SharedPreferences.Editor editor = prefs.edit();

                final Spinner feedItemSpinner = findViewById(R.id.feedItem);
                final Spinner feedRefreshSpinner = findViewById(R.id.feedRefresh);
                final TextView urlText = findViewById(R.id.rssUrl);

                final int feedItem = feedItemSpinner.getSelectedItemPosition();
                final int feedRefresh = feedRefreshSpinner.getSelectedItemPosition();
                final String url = urlText.getText().toString();

                // Will not save if not a valid url
                if (Patterns.WEB_URL.matcher(url).matches()) {
                    editor.putInt("feedItem", feedItem);
                    editor.putInt("feedRefresh", feedRefresh);
                    editor.putString("Url", url);

                    editor.apply();
                    finish();
                }
                else {
                    urlText.setError("Url is not valid!");
                }
            }
        });
    }
}
